package com.kit.quality.scenario;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Root
@Entity
@Table(schema = "dqs")
public class Scenario extends AbstractEntity<Long> {

    @Attribute
    @Column(unique = true, nullable = false)
    private String name;

    @Element
    @Column(nullable = false)
    private String entityName;

    @ElementList
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "scenario_rule", schema = "dqs",
            joinColumns = {@JoinColumn(name = "scenario_id")},
            inverseJoinColumns = {@JoinColumn(name = "rule_id")})
    private List<Rule> rules;

    @OneToMany(mappedBy = "scenario")
    private List<ScenarioResult> scenarioResults;

    @OneToMany(mappedBy = "scenario")
    private List<ScenarioResult> schedules;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Rule> getRules() {
        if (rules == null) {
            rules = new ArrayList<>();
        }
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public List<ScenarioResult> getScenarioResults() {
        return scenarioResults;
    }

    public void setScenarioResults(List<ScenarioResult> scenarioResults) {
        this.scenarioResults = scenarioResults;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<ScenarioResult> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<ScenarioResult> schedules) {
        this.schedules = schedules;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("entityName", entityName)
                .add("rules", rules)
//                .add("scenarioResults", scenarioResults)
                .toString();
    }
}
