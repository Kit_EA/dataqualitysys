package com.kit.quality.scenario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "dqs")
public class ScenarioResult extends AbstractEntity<Long> {

    @Column
    private double qualityIndex;

    @Column
    private Date launchDate;

    @Column
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "scenario_id")
    private Scenario scenario;

    @OneToMany(mappedBy = "scenarioResult", cascade = {CascadeType.PERSIST})
    private List<WrongData> wrongData;

    public Scenario getScenario() {
        return scenario;
    }

    public List<WrongData> getWrongData() {
        if (wrongData == null) {
            wrongData = new ArrayList<>();
        }
        return wrongData;
    }

    public double getQualityIndex() {
        return qualityIndex;
    }

    public Date getLaunchDate() {
        return launchDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public void setWrongData(List<WrongData> wrongData) {
        this.wrongData = wrongData;
    }

    public void setQualityIndex(double qualityIndex) {
        this.qualityIndex = qualityIndex;
    }

    public void setLaunchDate(Date launchDate) {
        this.launchDate = launchDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("qualityIndex", qualityIndex)
                .add("launchDate", launchDate)
                .add("endDate", endDate)
                .add("scenario", scenario)
                .add("wrongData", wrongData)
                .toString();
    }
}
