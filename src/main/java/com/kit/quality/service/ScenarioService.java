package com.kit.quality.service;

import com.google.common.collect.Lists;
import com.kit.quality.foundation.ScenarioExecutor;
import com.kit.quality.foundation.XMLSerializer;
import com.kit.quality.scenario.*;
import com.kit.quality.scenario.repository.RuleRepository;
import com.kit.quality.scenario.repository.ScenarioRepository;
import com.kit.quality.scenario.repository.ScenarioResultRepository;
import com.kit.quality.scenario.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ScenarioService {

    @Autowired private RuleRepository ruleRepository;
    @Autowired private ScenarioRepository scenarioRepository;
    @Autowired private ScenarioResultRepository scenarioResultRepository;
    @Autowired private ScheduleRepository scheduleRepository;

    @Autowired private ScenarioExecutor scenarioExecutor;

    public Scenario findScenario(String scenarioName) {
        return scenarioRepository.findByName(scenarioName);
    }

    public Rule findRule(String ruleName) {
        return ruleRepository.findByName(ruleName);
    }

    public Schedule findSchedule(String schedule) {
        return scheduleRepository.findByName(schedule);
    }

    public List<Rule> findAllRules() {
        return Lists.newArrayList(ruleRepository.findAll());
    }

    public Rule addRule(Rule rule) {
        return ruleRepository.save(rule);
    }

    public Scenario addScenario(Scenario scenario) {
        return scenarioRepository.save(scenario);
    }

    public ScenarioResult addScenarioResult(ScenarioResult scenarioResult) {
        return  scenarioResultRepository.save(scenarioResult);
    }

    public List<Scenario> findAllScenarios() {
        return Lists.newArrayList(scenarioRepository.findAll());
    }

    public void runScenario(Scenario scenario) {
        ScenarioResult scenarioResult = scenarioExecutor.runScenario(scenario);
        scenarioResultRepository.save(scenarioResult);
    }

    @Transactional
    public void runScenario(String scenarioName){
        Scenario scenario = findScenario(scenarioName);
        runScenario(scenario);
    }

    public void parseXml(String file) {
        Configuration configuration = new XMLSerializer<Configuration>().read(file, Configuration.class);

        scenarioRepository.save(configuration.getScenarios());
        scheduleRepository.save(configuration.getSchedules());
    }
}
