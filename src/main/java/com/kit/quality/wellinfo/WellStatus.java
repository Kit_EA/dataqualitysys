package com.kit.quality.wellinfo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "wellinfo")
public class WellStatus extends AbstractEntity<Long> {

    @Column
    private String name;

    @OneToMany(mappedBy = "wellStatus")
    private List<Well> wells;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Well> getWells() {
        return wells;
    }

    public void setWells(List<Well> wells) {
        this.wells = wells;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
//                .add("wells", wells)
                .toString();
    }
}
