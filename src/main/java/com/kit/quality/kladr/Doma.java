package com.kit.quality.kladr;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "doma", schema = "kladr")
public class Doma extends AbstractEntity<Integer> {

    @Column
    private String name;

    @Column
    private String korp;

    @Column
    private String socr;

    @Column(precision = 19, scale = 0)
    private BigDecimal code;

    @Column
    private int index;

    @Column
    private int gninmb;

    @Column
    private int uno;

    @Column
    private long ocatd;

    public String getName() {
        return name;
    }

    public String getKorp() {
        return korp;
    }

    public String getSocr() {
        return socr;
    }

    public BigDecimal getCode() {
        return code;
    }

    public int getIndex() {
        return index;
    }

    public int getGninmb() {
        return gninmb;
    }

    public int getUno() {
        return uno;
    }

    public long getOcatd() {
        return ocatd;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKorp(String korp) {
        this.korp = korp;
    }

    public void setSocr(String socr) {
        this.socr = socr;
    }

    public void setCode(BigDecimal code) {
        this.code = code;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setGninmb(int gninmb) {
        this.gninmb = gninmb;
    }

    public void setUno(int uno) {
        this.uno = uno;
    }

    public void setOcatd(long ocatd) {
        this.ocatd = ocatd;
    }
}
