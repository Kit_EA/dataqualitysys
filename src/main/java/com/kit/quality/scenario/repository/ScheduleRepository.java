package com.kit.quality.scenario.repository;

import com.kit.quality.scenario.Schedule;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleRepository extends CrudRepository<Schedule, Long> {

    Schedule findByName(String name);
}
