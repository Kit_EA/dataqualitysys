package com.kit.quality.scenario.repository;

import com.kit.quality.scenario.ScenarioResult;
import org.springframework.data.repository.CrudRepository;

public interface ScenarioResultRepository extends CrudRepository<ScenarioResult, Long> {

}
