package com.kit.quality.wellinfo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "enterprise_field", schema = "wellinfo")
public class EnterpriseField extends AbstractEntity<Long> {

    @OneToMany(mappedBy = "enterpriseField")
    private List<Well> wells;
}
