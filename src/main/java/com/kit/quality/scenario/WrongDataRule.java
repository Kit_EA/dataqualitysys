package com.kit.quality.scenario;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "wrong_data_rule", schema = "dqs")
public class WrongDataRule extends AbstractEntity<Long> {
}
