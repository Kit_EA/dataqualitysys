package com.kit.quality.scenario;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "scenario_rule", schema = "dqs")
public class ScenarioRule extends AbstractEntity<Long> {

}
