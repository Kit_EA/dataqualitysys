package com.kit.quality.kladr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "street", schema = "kladr")
public class Street extends AbstractEntity<Integer> {

    @Column
    private String name;

    @Column
    private String socr;

    @Column
    private long code;

    @Column
    private int index;

    @Column
    private int gninmb;

    @Column
    private int uno;

    @Column
    private long ocatd;

    public String getName() {
        return name;
    }

    public String getSocr() {
        return socr;
    }

    public long getCode() {
        return code;
    }

    public int getIndex() {
        return index;
    }

    public int getGninmb() {
        return gninmb;
    }

    public int getUno() {
        return uno;
    }

    public long getOcatd() {
        return ocatd;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSocr(String socr) {
        this.socr = socr;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setGninmb(int gninmb) {
        this.gninmb = gninmb;
    }

    public void setUno(int uno) {
        this.uno = uno;
    }

    public void setOcatd(long ocatd) {
        this.ocatd = ocatd;
    }
}
