package com.kit.quality.scenario;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root
public class Configuration {

    @ElementList
    private List<Scenario> scenarios;

    @ElementList
    private List<Schedule> schedules;

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setScenarios(List<Scenario> scenarios) {
        this.scenarios = scenarios;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }
}
