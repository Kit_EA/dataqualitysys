package com.kit.quality.kladr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "flat", schema = "kladr")
public class Flat extends AbstractEntity<Integer> {

    @Column
    private long code;

    @Column
    private int np;

    @Column
    private int gninmb;

    @Column
    private String name;

    @Column
    private int index;

    @Column
    private int uno;

    public long getCode() {
        return code;
    }

    public int getNp() {
        return np;
    }

    public int getGninmb() {
        return gninmb;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public int getUno() {
        return uno;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public void setNp(int np) {
        this.np = np;
    }

    public void setGninmb(int gninmb) {
        this.gninmb = gninmb;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setUno(int uno) {
        this.uno = uno;
    }
}
