package com.kit.quality.employee.repository;

import com.kit.quality.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class EmployeeRepositoryTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void queryEmployee() throws Exception {
        List resultList = em.createQuery("from Employee").getResultList();
        resultList.stream().forEach(System.out::println);

        resultList = em.createQuery("from Well").getResultList();
        resultList.stream().forEach(System.out::println);
    }
}