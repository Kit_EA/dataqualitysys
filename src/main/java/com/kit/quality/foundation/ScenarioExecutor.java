package com.kit.quality.foundation;

import com.kit.quality.scenario.Scenario;
import com.kit.quality.scenario.ScenarioResult;
import com.kit.quality.scenario.WrongData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.kit.quality.foundation.EntityScanner.Location.ALL;

@Component
public class ScenarioExecutor {

    @PersistenceContext
    private EntityManager em;

    @Autowired private EntityScanner entityScanner;

    private ExpressionParser parser = new SpelExpressionParser();

    public ScenarioResult runScenario(Scenario scenario) {
        ScenarioResult scenarioResult = new ScenarioResult();
        scenarioResult.setScenario(scenario);
        scenarioResult.setLaunchDate(new Date());

        String entityName = scenario.getEntityName();
        Class<?> entityClass = getEntityClass(entityName);

        List<?> resultList = getResultList(entityClass);
        resultList.forEach(entity -> {
            scenarioResult.getWrongData().addAll(
                    executeExpression(scenarioResult, scenario, entityClass, entity)); //scenarioResult,
        });

        scenarioResult.setQualityIndex(calculateQualityIndex(scenarioResult, resultList));

        scenarioResult.setEndDate(new Date());
        return scenarioResult;
    }

    private double calculateQualityIndex(ScenarioResult scenarioResult, List<?> resultList) {
        return 100 * (1.0 - ((double)scenarioResult.getWrongData().size() / (double) resultList.size()));
    }

    private Class<?> getEntityClass(String entityName) {
        return entityScanner.getEntityClass(ALL, entityName);
    }

    private List getResultList(Class<?> entityClass) {
        return em.createQuery("from " + entityClass.getSimpleName()).getResultList();
    }

    private List<WrongData> executeExpression(ScenarioResult scenarioResult, Scenario scenario, Class<?> entityClass, Object entity) { //ScenarioResult scenarioResult,
        List<WrongData> wrongDataList = new ArrayList<>();
        scenario.getRules().forEach(rule -> {
            if (!executeExpression(entity, entityClass, rule.getExpression())) {
                WrongData wrongData = new WrongData();
                wrongData.setWrongObject(entity.toString());
                wrongData.getRules().add(rule);
                wrongData.setScenarioResult(scenarioResult);
                wrongDataList.add(wrongData);
            }
        });

        return wrongDataList;
    }

    private Boolean executeExpression(Object entity, Class<?> entityClass, String expression) {
        EvaluationContext context = new StandardEvaluationContext(entity);
        Expression exp = parser.parseExpression(expression);
        return exp.getValue(context, Boolean.class);
    }
}
