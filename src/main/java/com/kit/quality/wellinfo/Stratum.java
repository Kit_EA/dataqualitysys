package com.kit.quality.wellinfo;

import java.util.List;

import javax.persistence.*;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "wellinfo")
public class Stratum extends AbstractEntity<Long> {

    @Column
    private String upperStratum;

    @Column
    private String name;

    @Column
    private String code;

    @OneToMany(mappedBy = "stratum")
    private List<Well> wells;

    public String getUpperStratum() {
        return upperStratum;
    }

    public void setUpperStratum(String upperStratum) {
        this.upperStratum = upperStratum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Well> getWells() {
        return wells;
    }

    public void setWells(List<Well> wells) {
        this.wells = wells;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("upperStratum", upperStratum)
                .add("name", name)
                .add("code", code)
//                .add("wells", wells)
                .toString();
    }
}
