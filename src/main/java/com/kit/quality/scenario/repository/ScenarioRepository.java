package com.kit.quality.scenario.repository;

import com.kit.quality.scenario.Scenario;
import org.springframework.data.repository.CrudRepository;

public interface ScenarioRepository extends CrudRepository<Scenario, Long> {

    Scenario findByName(String name);

}
