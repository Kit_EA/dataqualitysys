package com.kit.quality.employee.repository;

import com.kit.quality.scenario.Rule;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Rule, Long> {

}
