package com.kit.quality.scenario;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Root
@Entity
@Table(schema = "dqs")
public class Rule extends AbstractEntity<Long> {

    @Attribute
    @Column(unique = true, nullable = false)
    private String name;

    @Attribute(required = false)
    @Column
    private String description;

    @Attribute
    @Column(nullable = false)
    private String expression;

    @ManyToMany//(mappedBy = "Scenario")
    @JoinTable(name = "scenario_rule", schema = "dqs",
            joinColumns = {@JoinColumn(name = "rule_id")},
            inverseJoinColumns = {@JoinColumn(name = "scenario_id")})
    private List<Scenario> scenarios;

    @ManyToMany
    @JoinTable(name = "wrong_data_rule", schema = "dqs",
            joinColumns = {@JoinColumn(name = "rule_id")},
            inverseJoinColumns = {@JoinColumn(name = "wrong_data_id")})
    private List<WrongData> wrongData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<Scenario> scenarios) {
        scenarios = scenarios;
    }

    public List<WrongData> getWrongData() {
        return wrongData;
    }

    public void setWrongData(List<WrongData> wrongData) {
        this.wrongData = wrongData;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("description", description)
                .add("expression", expression)
//                .add("scenarios", scenarios)
//                .add("wrongData", wrongData)
                .toString();
    }
}
