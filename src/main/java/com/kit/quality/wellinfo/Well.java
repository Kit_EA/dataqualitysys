package com.kit.quality.wellinfo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "wellinfo")
public class Well extends AbstractEntity<Long> {

    @Column(name = "well_number")
    private int wellNumber;

    @Column(name = "date_end")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "stratum_id")
    private Stratum stratum;

    @ManyToOne
    @JoinColumn(name = "enterprise_field_id")
    private EnterpriseField enterpriseField;

    @ManyToOne
    @JoinColumn(name = "trunk_plot_id")
    private TrunkPlot trunkPlot;

    @ManyToOne
    @JoinColumn(name = "well_category_id")
    private WellCategory wellCategory;

    @ManyToOne
    @JoinColumn(name = "well_status_id")
    private WellStatus wellStatus;

    public int getWellNumber() {
        return wellNumber;
    }

    public void setWellNumber(int wellNumber) {
        this.wellNumber = wellNumber;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Stratum getStratum() {
        return stratum;
    }

    public void setStratum(Stratum stratum) {
        this.stratum = stratum;
    }

    public EnterpriseField getEnterpriseField() {
        return enterpriseField;
    }

    public void setEnterpriseField(EnterpriseField enterpriseField) {
        this.enterpriseField = enterpriseField;
    }

    public TrunkPlot getTrunkPlot() {
        return trunkPlot;
    }

    public void setTrunkPlot(TrunkPlot trunkPlot) {
        this.trunkPlot = trunkPlot;
    }

    public WellCategory getWellCategory() {
        return wellCategory;
    }

    public void setWellCategory(WellCategory wellCategory) {
        this.wellCategory = wellCategory;
    }

    public WellStatus getWellStatus() {
        return wellStatus;
    }

    public void setWellStatus(WellStatus wellStatus) {
        this.wellStatus = wellStatus;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("wellNumber", wellNumber)
                .add("endDate", endDate)
                .add("stratum", stratum.getId())
                .add("enterpriseField", enterpriseField.getId())
                .add("trunkPlot", trunkPlot.getId())
                .add("wellCategory", wellCategory.getId())
                .add("wellStatus", wellStatus.getId())
                .toString();
    }
}
