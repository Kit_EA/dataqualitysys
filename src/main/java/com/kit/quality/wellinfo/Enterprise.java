package com.kit.quality.wellinfo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "wellinfo")
public class Enterprise extends AbstractEntity<Long> {

    @Column
    private String name;

    @Column
    private String code;

    @ManyToMany
    @JoinTable(name = "enterprise_field", schema = "wellinfo",
            joinColumns = {@JoinColumn(name = "enterprise_id")},
            inverseJoinColumns = {@JoinColumn(name = "field_id")})
    private List<Field> fields;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("code", code)
//                .add("wells", wells)
                .add("fields", fields)
                .toString();
    }
}
