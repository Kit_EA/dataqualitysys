package com.kit.quality.scenario;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "dqs")
public class WrongData extends AbstractEntity<Long> {

    @Column
    private String wrongObject;

    @ManyToOne
    private ScenarioResult scenarioResult;

    @ManyToMany
    @JoinTable(name = "wrong_data_rule", schema = "dqs",
            joinColumns = {@JoinColumn(name = "wrong_data_id")},
            inverseJoinColumns = {@JoinColumn(name = "rule_id")})
    private List<Rule> rules;

    public String getWrongObject() {
        return wrongObject;
    }

    public void setWrongObject(String wrongObject) {
        this.wrongObject = wrongObject;
    }

    public ScenarioResult getScenarioResult() {
        return scenarioResult;
    }

    public void setScenarioResult(ScenarioResult scenarioResult) {
        this.scenarioResult = scenarioResult;
    }

    public List<Rule> getRules() {
        if (rules == null) {
            rules = new ArrayList<>();
        }
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("wrongObject", wrongObject)
                .add("scenarioResult", scenarioResult)
                .add("rules", rules)
                .toString();
    }
}
