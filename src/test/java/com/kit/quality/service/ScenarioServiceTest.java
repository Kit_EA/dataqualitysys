package com.kit.quality.service;

import com.kit.quality.Application;
import com.kit.quality.foundation.PathExtractor;
import com.kit.quality.foundation.ScenarioExecutor;
import com.kit.quality.scenario.*;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.CycleStrategy;
import org.simpleframework.xml.strategy.Strategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ScenarioServiceTest {

    @Autowired
    private ScenarioService scenarioService;

    @Test
    public void findScenario() throws Exception {

    }

    @Test
    public void findRule() throws Exception {

    }

    @Test
    public void addRule() throws Exception {
        Rule rule = new Rule();
        rule.setName("Rule_1");
        rule.setDescription("Some description");
        rule.setExpression("Some expression");

        Rule savedRule = scenarioService.addRule(rule);

        Rule foundRule = scenarioService.findRule("Rule_1");

        Assert.assertEquals(savedRule, foundRule);
    }

    @Test
    public void parseXml() throws Exception {
        PathExtractor pE = new PathExtractor();
        Path fileLocation = pE.getPathToFile("xmlSchem.xml");

        File inputFile = new File(fileLocation.toString());
        SAXReader reader = new SAXReader();
        Document document = reader.read(inputFile);
        List<Node> nodes = document.selectNodes("/dqs/rules/rule");

        Node node = nodes.get(0);

        Rule rule = new Rule();
        rule.setName(node.valueOf("@name"));
        rule.setDescription(node.selectSingleNode("description").getText());
        rule.setExpression(node.selectSingleNode("expression").getText());
        Rule savedRule = scenarioService.addRule(rule);
        Rule foundRule = scenarioService.findRule("codeLengthKladr");

        Assert.assertEquals(savedRule, foundRule);
    }

    @Test
    public void parseDeserializeSimpleXml() throws Exception{
        PathExtractor pE = new PathExtractor();
        Path fileLocation = pE.getPathToFile("xmlSchem.xml");
        Serializer serializer = new Persister();
        File source = new File(fileLocation.toString());

        Configuration configuration = serializer.read(Configuration.class, source);

        Scenario scenario = configuration.getScenarios().get(0);
        List<Rule> ruleList = scenario.getRules();
    }

    @Test
    public void parseSerializeSimpleXml() throws Exception{
        PathExtractor pE = new PathExtractor();
        Path fileLocation = pE.getPathToFile("testXML.xml");

        Strategy strategy = new CycleStrategy("id", "ref");
        Serializer serializer = new Persister(strategy);
        Rule rule = new Rule();
        rule.setName("codeLengthKladr");
        rule.setDescription("code length check");
        rule.setExpression("code matches [0-9]{13,14}");

        Rule rule1 = new Rule();
        rule1.setName("codeLengthStreet");
        rule1.setDescription("code length check");
        rule1.setExpression("code matches [0-9]{17,18}");

        Rule rule2 = new Rule();
        rule2.setName("indexLength");
        rule2.setDescription("index symb match");
        rule2.setExpression("index matches [0-9]{6,7}");

        Rule rule3 = new Rule();
        rule3.setName("gninmbLength");
        rule3.setDescription("gnimb symb match");
        rule3.setExpression("gninmb matches [0-9]{3,4}");

        List<Rule> ruleList = new ArrayList<>();
        ruleList.add(rule);
        ruleList.add(rule2);

        List<Rule> ruleList1 = new ArrayList<>();
        ruleList1.add(rule1);
        ruleList1.add(rule3);

        Scenario scenario = new Scenario();
        scenario.setName("Check kladr consistence");
        scenario.setEntityName("Kladr");
        scenario.setRules(ruleList);

        Scenario scenario1 = new Scenario();
        scenario1.setName("Check street consistence");
        scenario1.setEntityName("Street");
        scenario1.setRules(ruleList1);

        List<Scenario> scenarioList = new ArrayList<>();
        scenarioList.add(scenario);
        scenarioList.add(scenario1);

        Schedule schedule = new Schedule();
        schedule.setName("one");
        schedule.setScenario(scenario);
//        schedule.setPeriod(3600);

        Schedule schedule1 = new Schedule();
        schedule1.setName("two");
        schedule1.setScenario(scenario1);
//        schedule1.setPeriod(4000);

        List<Schedule> scheduleList = new ArrayList<>();
        scheduleList.add(schedule);
        scheduleList.add(schedule1);

        Configuration configuration = new Configuration();
        configuration.setScenarios(scenarioList);
        configuration.setSchedules(scheduleList);

        File result = new File(fileLocation.toString());
        serializer.write(configuration, result);
    }

    @Test
    public void testParserComplete() throws Exception {
        PathExtractor pE = new PathExtractor();
        //Path fileLocation = pE.getPathToFile("xmlSchem.xml");
        //Path fileLocation = pE.getPathToFile("testParseXML.xml");
        Path fileLocation = pE.getPathToFile("xmlWellinfoSchema.xml");

        scenarioService.parseXml(fileLocation.toString());
//        Scenario scenario = scenarioService.findScenario("Check consistence");


//        Rule rule = scenarioService.findRule("Employee max");
//        Schedule schedule = scenarioService.findSchedule("one");
    }

    @Test
    public void runSceanrio() throws Exception{
        //scenarioService.runScenario("Check consistence");
        scenarioService.runScenario("Well check");
    }

    @Test
    public void returnRules() throws Exception {
        List<Rule> allRules = scenarioService.findAllRules();

        for (Rule rule : allRules) {
            System.out.println(rule.getName());
        }
    }
}