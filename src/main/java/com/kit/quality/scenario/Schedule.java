package com.kit.quality.scenario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Root
@Entity
@Table(schema = "dqs")
public class Schedule extends AbstractEntity<Long> {

    @Attribute
    @Column
    private String name;

    @Element
    @ManyToOne
    @JoinColumn(name = "scenario_id")
    private Scenario scenario;

    @Element
    @Column
    private String cron;

    @Column
    private long fixedDelay;

    @Column
    private long fixedRate;

    @Column
    private long initialDelay;

    public String getName() {
        return name;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public long getFixedDelay() {
        return fixedDelay;
    }

    public void setFixedDelay(long fixedDelay) {
        this.fixedDelay = fixedDelay;
    }

    public long getFixedRate() {
        return fixedRate;
    }

    public void setFixedRate(long fixedRate) {
        this.fixedRate = fixedRate;
    }

    public long getInitialDelay() {
        return initialDelay;
    }

    public void setInitialDelay(long initialDelay) {
        this.initialDelay = initialDelay;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("scenario", scenario)
                .add("cron", cron)
                .add("fixedDelay", fixedDelay)
                .add("fixedRate", fixedRate)
                .add("initialDelay", initialDelay)
                .toString();
    }
}
