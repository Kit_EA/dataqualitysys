package com.kit.quality.foundation;

import org.reflections.Reflections;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.util.Set;

@Component
public class EntityScanner {

    public enum Location {
        ALL("com.kit.quality"),
        DQS("com.kit.quality.scenario"),
        WELLINFO("com.kit.quality.wellinfo"),
        KLADR("com.kit.quality.kladr");

        private String path;

        Location(String path) {
            this.path = path;
        }
    }

    private Reflections getReflections(Location location) {
        return new Reflections(location.path);
    }

    public Set<Class<?>> getEntityClasses(Location location) {
        return getReflections(location).getTypesAnnotatedWith(Entity.class);
    }

    public Class<?> getEntityClass(Location location, String name) {
        return getEntityClasses(location).stream()
                .filter(aClass -> aClass.getSimpleName().equals(name))
                .findFirst().get();
    }

}
