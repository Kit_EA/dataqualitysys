package com.kit.quality.kladr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "altnames", schema = "kladr")
public class AltNames extends AbstractEntity<Integer> {

    @Column(name = "oldcode")
    private long oldCode;

    @Column(name = "newcode")
    private long newCode;

    @Column
    private int level;

    public long getOldCode() {
        return oldCode;
    }

    public long getNewCode() {
        return newCode;
    }

    public int getLevel() {
        return level;
    }

    public void setOldCode(long oldCode) {
        this.oldCode = oldCode;
    }

    public void setNewCode(long newCode) {
        this.newCode = newCode;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
