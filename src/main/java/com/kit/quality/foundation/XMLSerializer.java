package com.kit.quality.foundation;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.CycleStrategy;
import org.simpleframework.xml.strategy.Strategy;

import java.io.File;

public class XMLSerializer<T> {

    private Strategy strategy = new CycleStrategy("id", "ref");
    private Serializer serializer = new Persister(strategy);

    public T read(String file, Class<T> type) {
        File source = new File(file);

        try {
            return serializer.read(type, source);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void write(String file, T object) {
        File result = new File(file);

        try {
            serializer.write(object, result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
