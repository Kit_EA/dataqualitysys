package com.kit.quality.wellinfo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "wellinfo")
public class TrunkPlot extends AbstractEntity<Long> {

    @Column(name = "name_short")
    private String shortName;

    @Column
    private String name;

    @Column
    private String code;

    @OneToMany(mappedBy = "trunkPlot")
    private List<Well> wells;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Well> getWells() {
        return wells;
    }

    public void setWells(List<Well> wells) {
        this.wells = wells;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("shortName", shortName)
                .add("name", name)
                .add("code", code)
//                .add("wells", wells)
                .toString();
    }
}
