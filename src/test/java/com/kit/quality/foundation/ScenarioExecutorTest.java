package com.kit.quality.foundation;

import com.kit.quality.Application;
import com.kit.quality.scenario.Rule;
import com.kit.quality.scenario.Scenario;
import com.kit.quality.scenario.ScenarioResult;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ScenarioExecutorTest {

    @Autowired private ScenarioExecutor scenarioExecutor;

    @Test
    public void runScenario() throws Exception {
        Rule rule = new Rule();
        rule.setExpression("salary > 3000");

        Scenario scenario = new Scenario();
        scenario.setEntityName("Employee");
        scenario.getRules().add(rule);

        ScenarioResult scenarioResult = scenarioExecutor.runScenario(scenario);

        Assert.assertThat(scenarioResult.getWrongData().size(), is(4));
    }
}