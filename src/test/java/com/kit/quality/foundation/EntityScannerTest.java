package com.kit.quality.foundation;

import com.kit.quality.Application;
import com.kit.quality.employee.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import static com.kit.quality.foundation.EntityScanner.Location.ALL;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class EntityScannerTest {

    @Autowired private EntityScanner entityScanner;

    @Test
    public void getEntityClass() throws Exception {
        Set<Class<?>> entityClasses = entityScanner.getEntityClasses(ALL);
        entityClasses.forEach(System.out::println);

        Class<?> aClass = entityScanner.getEntityClass(ALL, "Employee");
        Assert.assertEquals(Employee.class, aClass);
    }
}