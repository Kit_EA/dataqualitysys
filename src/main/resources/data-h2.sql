-- Employee
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (1, 'Tom', 'Sawyer', 3000);
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (2, 'Max', 'Pain', 2000);
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (3, 'Jim', 'Brain', 3500);
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (4, 'Mad', 'Potter', 6700);
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (5, 'Max', 'Power', 2500);
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (6, 'Jan', 'Jenjens', 4000);
INSERT INTO DQS.EMPLOYEE(ID, FIRST_NAME, LAST_NAME, SALARY) VALUES (7, 'Sam', 'Bigens', 3000);

---- Enterprise
INSERT INTO WELLINFO.ENTERPRISE(ID, NAME, CODE) VALUES(422,'ТПП "Когалымнефтегаз"','301');
INSERT INTO WELLINFO.ENTERPRISE(ID, NAME, CODE) VALUES(745,'ТПП "Повхнефтегаз"','001');
--
---- Field
INSERT INTO WELLINFO.FIELD(ID, NAME_SHORT, NAME, CODE) VALUES(48,'VAT','Ватьеганское','317');
INSERT INTO WELLINFO.FIELD(ID, NAME_SHORT, NAME, CODE) VALUES(88,'POV','Повховское','326');
INSERT INTO WELLINFO.FIELD(ID, NAME_SHORT, NAME, CODE) VALUES(117,'TVR','Тевлинско-Русскинское','617');
INSERT INTO WELLINFO.FIELD(ID, NAME_SHORT, NAME, CODE) VALUES(137,'UYG','Южно-Ягунское','293');
--
---- EnterpriseField
INSERT INTO WELLINFO.ENTERPRISE_FIELD(ID, ENTERPRISE_ID, FIELD_ID) VALUES(323,422,117);
INSERT INTO WELLINFO.ENTERPRISE_FIELD(ID, ENTERPRISE_ID, FIELD_ID) VALUES(327,422,137);
INSERT INTO WELLINFO.ENTERPRISE_FIELD(ID, ENTERPRISE_ID, FIELD_ID) VALUES(401,745,48);
INSERT INTO WELLINFO.ENTERPRISE_FIELD(ID, ENTERPRISE_ID, FIELD_ID) VALUES(402,745,88);
--
---- WellCategory
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(1,'Pr','Параметрические','01');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(2,'Ps','Поисковая','02');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(3,'Rz','Разведочная','03');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(4,'Ek','Эксплуатационная нефтяная','04');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(5,'Ng','Нагнетательная','05');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(6,'Nb','Наблюдательные','06');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(7,'Pz','Пьезометрические','07');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(8,'Oc','Оценочные','08');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(9,'Vd','Водозаборные','09');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(10,'Pg','Поглощающие','10');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(11,'Db','Дублирующие','11');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(12,'Eg','Эксплуатационная  газовая','12');
INSERT INTO WELLINFO.WELL_CATEGORY(ID, SYMBOL, NAME, CODE) VALUES(13,'Egk','Эксплуатационная-газоконденсат','13');

---- TrunkPlot
INSERT INTO WELLINFO.TRUNK_PLOT(ID, NAME_SHORT, NAME, CODE) VALUES(1,NULL,'Нет данных','00');
INSERT INTO WELLINFO.TRUNK_PLOT(ID, NAME_SHORT, NAME, CODE) VALUES(2,NULL,'Вертикальный','01');
INSERT INTO WELLINFO.TRUNK_PLOT(ID, NAME_SHORT, NAME, CODE) VALUES(3,'ННС','Наклонно - направленный','02');
INSERT INTO WELLINFO.TRUNK_PLOT(ID, NAME_SHORT, NAME, CODE) VALUES(4,'ГС','Горизонтальный','03');
INSERT INTO WELLINFO.TRUNK_PLOT(ID, NAME_SHORT, NAME, CODE) VALUES(5,'ПР','Прочие','04');

---- WellStatus
INSERT INTO WELLINFO.WELL_STATUS(ID, NAME) VALUES(0,'Не определёно');
INSERT INTO WELLINFO.WELL_STATUS(ID, NAME) VALUES(1,'Действующий фонд');
INSERT INTO WELLINFO.WELL_STATUS(ID, NAME) VALUES(2,'Проектный фонд');

---- Stratum
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(1,NULL,'Ач1/0-БВ7','R067');
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(2,NULL,'ЮШ6+ЮШ7','349');
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(3,NULL,'Ач2/1','096');
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(4,NULL,'АВ1','001');
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(5,NULL,'АВ2/1','017');
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(6,'001','АВ1+АВ2','021');
insert into WELLINFO.STRATUM(ID, UPPER_STRATUM, NAME, CODE) VALUES(7,'001','АВ1+АВ3','499');

---- Well
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(815,401,3,4,1,1,TO_DATE('2000-12-21 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1497');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(703,401,3,4,1,1,TO_DATE('2000-11-21 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'3037');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(832,401,3,4,1,1,TO_DATE('2000-10-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'781');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(2323,327,3,4,1,2,TO_DATE('2000-06-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2108');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(2691,327,3,4,1,2,TO_DATE('2000-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1122');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(11529,327,3,4,1,2,TO_DATE('2000-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2106');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(11930,327,3,4,1,2,TO_DATE('2000-04-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'384');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(16213,327,3,4,1,3,TO_DATE('1999-10-28 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'359');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(9451,401,3,4,1,4,TO_DATE('1999-06-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5230');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(18073,327,3,4,1,4,TO_DATE('1999-05-22 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2104');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(18392,401,3,4,1,3,TO_DATE('1999-04-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2874');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(6252,401,3,4,1,5,TO_DATE('1999-03-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5396');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(9008,401,3,4,1,3,TO_DATE('1999-01-14 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5271');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(18068,327,3,4,1,5,TO_DATE('1999-01-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2128');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(15419,327,3,4,1,1,TO_DATE('1998-12-24 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1596');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(15850,401,3,4,1,2,TO_DATE('1998-12-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'8013');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(17444,402,3,4,1,4,TO_DATE('1998-08-24 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1070');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(3717,327,3,4,1,6,TO_DATE('1998-08-17 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'600');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(2670,327,3,4,1,2,TO_DATE('1998-08-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1642');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(10721,402,3,4,1,1,TO_DATE('1998-07-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'406');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8287,401,3,4,1,1,TO_DATE('1998-06-07 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'7106');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(13834,401,3,4,1,2,TO_DATE('1998-06-03 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'7078');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(11916,327,3,4,1,2,TO_DATE('1998-06-03 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'577');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(3187,327,3,4,1,3,TO_DATE('1998-06-03 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5074');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(3212,327,3,4,1,1,TO_DATE('1998-06-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2675');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(5309,402,3,4,1,2,TO_DATE('1998-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1800');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(16473,401,3,4,1,3,TO_DATE('1998-06-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1691');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(6820,401,3,4,1,1,TO_DATE('1998-05-25 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'4091');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8700,401,3,4,1,3,TO_DATE('1998-05-18 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2796');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8925,402,3,4,1,4,TO_DATE('1998-05-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'184');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8149,401,3,4,1,1,TO_DATE('1998-05-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5951');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(9921,401,3,4,1,2,TO_DATE('1998-04-27 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1405');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(18606,401,3,4,1,1,TO_DATE('1998-04-05 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2041');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(15758,327,3,4,1,2,TO_DATE('1998-04-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2409');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(432,327,3,4,1,1,TO_DATE('1998-02-04 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5059');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(13202,401,3,4,1,4,TO_DATE('1998-02-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1837');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(776,401,3,4,1,1,TO_DATE('1998-01-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2053');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(7840,402,3,4,1,2,TO_DATE('1998-01-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2343');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(15887,327,3,5,1,1,TO_DATE('1998-01-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'9640');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8919,402,3,4,1,1,TO_DATE('1998-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'209');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(904,401,3,4,1,2,TO_DATE('1998-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'222');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8806,402,3,4,1,2,TO_DATE('1998-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'303');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8697,401,3,4,1,3,TO_DATE('1997-12-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2846');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(2599,401,3,4,1,2,TO_DATE('1997-12-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'446');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(16906,402,3,4,1,4,TO_DATE('1997-11-19 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'3109');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(11483,327,3,4,1,5,TO_DATE('1997-11-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'3455');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(15124,327,3,4,1,1,TO_DATE('1997-10-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2037');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8191,401,3,4,1,2,TO_DATE('1997-09-29 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5312');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(9757,327,3,4,1,4,TO_DATE('1997-09-23 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'403');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(907,401,3,4,1,5,TO_DATE('1997-09-17 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'210');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8746,402,3,4,1,2,TO_DATE('1997-09-12 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1052');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(11516,327,3,4,1,1,TO_DATE('1997-08-18 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2247');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(15130,327,3,13,1,2,TO_DATE('1997-08-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1889');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(9295,401,3,4,1,4,TO_DATE('1997-07-20 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1851');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(802,401,3,4,1,2,TO_DATE('1997-07-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1692');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(13894,401,3,4,1,3,TO_DATE('1997-07-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'5876');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(5872,402,3,4,1,1,TO_DATE('1997-07-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'276');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(18608,401,3,4,1,3,TO_DATE('1997-06-26 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1977');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(6273,402,3,4,1,1,TO_DATE('1997-06-24 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'4816');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8260,401,3,4,1,2,TO_DATE('1997-06-17 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'8262');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(2627,401,3,4,1,4,TO_DATE('1997-06-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'218');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(16650,402,3,4,1,5,TO_DATE('1997-05-30 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'4449');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(9328,401,3,4,1,2,TO_DATE('1997-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1441');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(8749,402,3,4,1,1,TO_DATE('1996-12-27 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1024');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(726,401,3,4,1,4,TO_DATE('1996-12-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'2791');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(2505,401,3,4,1,6,TO_DATE('1996-11-27 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1767');
INSERT INTO WELLINFO.WELL(ID, ENTERPRISE_FIELD_ID, TRUNK_PLOT_ID, WELL_CATEGORY_ID, WELL_STATUS_ID, STRATUM_ID, DATE_END, WELL_NUMBER) VALUES(7922,402,3,4,1,2,TO_DATE('1996-10-18 00:00:00', 'YYYY-MM-DD HH24:MI:SS'),'1236');
