package com.kit.quality.wellinfo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(schema = "wellinfo")
public class Field extends AbstractEntity<Long> {

    @Column(name = "name_short")
    private String shortName;

    @Column
    private String name;

    @Column
    private String code;

    @ManyToMany
    @JoinTable(name = "enterprise_field", schema = "wellinfo",
            joinColumns = {@JoinColumn(name = "field_id")},
            inverseJoinColumns = {@JoinColumn(name = "enterprise_id")})
    private List<Enterprise> enterprises;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("shortName", shortName)
                .add("name", name)
                .add("code", code)
//                .add("wells", wells)
                .add("enterprises", enterprises)
                .toString();
    }
}
