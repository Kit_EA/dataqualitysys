package com.kit.quality;

import com.kit.quality.foundation.ScenarioExecutor;
import com.kit.quality.scenario.Rule;
import com.kit.quality.scenario.Scenario;
import com.kit.quality.scenario.ScenarioResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScenarioExecutorRun {

    @Autowired private ScenarioExecutor scenarioExecutor;

    //    @PostConstruct
    public void init() {
        Rule rule = new Rule();
        rule.setExpression("salary > 3000");

        Scenario scenario = new Scenario();
        scenario.setEntityName("Employee");
        scenario.getRules().add(rule);

        ScenarioResult scenarioResult = scenarioExecutor.runScenario(scenario);
    }
}
