package com.kit.quality.kladr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kit.quality.foundation.AbstractEntity;

@Entity
@Table(name = "socrbase", schema = "kladr")
public class Socrbase extends AbstractEntity<Integer> {

    @Column
    private int level;

    @Column
    private String scname;

    @Column
    private String socrname;

    @Column(name = "kod_t_st")
    private int kodTSt;

    public int getLevel() {
        return level;
    }

    public String getScname() {
        return scname;
    }

    public String getSocrname() {
        return socrname;
    }

    public int getKodTSt() {
        return kodTSt;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setScname(String scname) {
        this.scname = scname;
    }

    public void setSocrname(String socrname) {
        this.socrname = socrname;
    }

    public void setKodTSt(int kodTSt) {
        this.kodTSt = kodTSt;
    }
}
