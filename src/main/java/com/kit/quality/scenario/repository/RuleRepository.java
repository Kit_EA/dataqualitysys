package com.kit.quality.scenario.repository;

import com.kit.quality.scenario.Rule;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RuleRepository extends CrudRepository<Rule, Long> {

    Rule findByName(String name);

}
